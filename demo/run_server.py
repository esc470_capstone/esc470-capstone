import sys
import os
import json
import pickle
import re

import time
import datetime

import serial           
import socket
import requests


HOST = socket.gethostname()
PORT = 8080
POST_URL = 'http://localhost:8080'
POST_HEADERS = {'Content-type': 'application/json', 'Accept': 'text/plain'}




if __name__=='__main__':

  #Create a socket object
  try:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print ("Socket successfully created")
  except socket.error as err:
    print ("Socket creation failed with error %s" %(err))
    sys.exit()

  sock.bind((HOST, PORT))        # Bind to the port

  sock.listen(5)                 # Wait for client connection.
  while True:
     conn, client_addr = sock.accept()     
     conn.close()                # Close the connection

