import sys
import os
import json
import pickle
import re

import time
import datetime

import serial           
import socket
import requests



SKIP_KEYS = ['Timestamp', 'id', 'T_int']
SAVE_FILE = '../MicroMarket/PricingTests/usage.json'

#POST_URL = 'http://localhost:8000/MicroMarketApp/GetData'
HOST = socket.gethostname()
#print(HOST)
PORT = 8080
POST_URL = 'http://localhost:3000/api/EnergyBlock'
POST_HEADERS = {'Content-type': 'application/json', 'Accept': 'application/json'}

def SendRequestFromMeter(Circuit_Data):
  # Based on the function in integration_test.py in the MicroMarket directory
  django_url = 'http://localhost:8000/MicroMarketApp/smart_contract'
    
  Alt_Data = {}    
    
  # Preprocessing
  for key in Circuit_Data:
        users_already_seen = []
        alt_time  = datetime.datetime.strptime(key, '%Y-%m-%d %H:%M:%S') + datetime.timedelta(seconds=1)   
        alt_datas = []

        for idx, usage in enumerate(Circuit_Data[key]):
            if usage["username"] not in users_already_seen:
                users_already_seen.append(usage["username"])
            else: # Need to change its timestamp
                data_to_move = Circuit_Data[key].pop(idx)
                alt_datas.append(data_to_move)

        Alt_Data[alt_time] = alt_datas
                
        Circuit_Data[key] = json.dumps(Circuit_Data[key]) # Format as str
 
  for key in Alt_Data:
        Alt_Data[key] = json.dumps(Alt_Data[key])                
                
  requests.post(django_url, data=Circuit_Data)
  requests.post(django_url, data=Alt_Data)
    
  return


if __name__=='__main__':
  #Read Serial Data
  ComPort = serial.Serial('/dev/ttyACM' + str(sys.argv[1])) #open Hardware Serial port
  ComPort.baudrate = 38400 # set Baud rate to 9600
  ComPort.bytesize = 8    # Number of data bits = 8
  ComPort.parity   = 'N'  # No parity
  ComPort.stopbits = 1    # Number of Stop bits = 1
  
#  #Connect to a port network
#  try:
#    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#    print ("Socket successfully created")
#  except socket.error as err:
#    print ("socket creation failed with error %s" %(err))
#  print(HOST)
#  sock.connect((HOST, PORT))

  usage_hash = {}
  try:
    while(1):
      #Get timestamp
      secs_since_1970 = time.time()
      timestamp = datetime.datetime.fromtimestamp(secs_since_1970). \
              strftime('%Y-%m-%d %H:%M:%S')

      #Read Serial port
      line = ComPort.readline() # must send \n! get a line of log
      line = line.decode("utf-8")
      line = line.rstrip("/\n")
      line = re.sub(r'^id:\s+\d+', 'Time: ' + timestamp, line)
      print (line)                 # show line on screen

      #Parse line for this timestamp and create an element for usage_hash dict
      #key = timestamp
      #val = Array --> elements = hash with contents below:
        #{username: [str]; amount: [int]; producing: [1 or 0]}
      timestamp_data = [] 
      items = line.split(";")
      for item in items[1:]: #Skip timestamp which has colons
        
        pair = item.split(":")
        key = pair[0].strip()
        val = pair[1].strip()
        if key in SKIP_KEYS:
          continue

        #If prosumer, remove the _c or _p part in their username
        parsed_user = re.sub(r'_[cp]', '', key)

        #Create a hash for each user's usage for this particular timestamp
        item_hash = {
          'username': parsed_user,
          'amount': float(val),
          'producing': int('_p' in key)
        }
        
        #Append this hash of user's usage to timestamp data
        timestamp_data.append(item_hash)
      
      #Add data from this timestamp ot overall usage hash which will be logged
      usage_hash[timestamp] = timestamp_data

      #Convert into JSON format and send over to specified URL
#      r = requests.post(POST_URL, data=json.dumps(timestamp_data), \
#            headers=POST_HEADERS)

  except KeyboardInterrupt:
    #Get path to file folder
    root_path = os.path.join(os.path.dirname(os.path.realpath(__file__)))
    save_pathname = os.path.join(root_path, SAVE_FILE)
    SendRequestFromMeter(usage_hash)
    
    print("\n\n")
    print ("Keyboard Interrupt: Saving power usage data into {}...". \
            format(save_pathname))
    with open(save_pathname, 'w') as f:
      json.dump(usage_hash, f, sort_keys=True, indent=4)



