//First version to test potentiometer to measure voltage
//drop across an LED and printing it on Serial Monitor
//Started March 2, 2018

//----------------------- Importing Libraries ----------------------------------------
#include "Timer.h"



//----------------------- Declaring Macros ----------------------------------------
//Arduino sample time to update LED brightness
const double T_SAMPLE = 0.03; //in seconds
//Data trasnfer rate to print data to Serial Monitor
const double T_TRANS = 1.5; //in seconds

//Potentiometer Analog Sensor Pins for Voltage Dividers (range = 0-->1023)
const int POT0 = A0; //Value across LED0
const int POT1 = A1; //Value across LED1
const int POT2 = A2; //Value across LED2
const int POT3 = A3; //Value across LED3
const int POT4 = A4; //Value across LED4
const int POT5 = A5; //Value across LED5
const int POT6 = A6; //Value across LED6
const int POT7 = A7; //Value across LED7
const int NUM_OF_POTS = 8;
const int POT_PINS[NUM_OF_POTS] = { POT0, POT1, POT2, POT3,
                                    POT4, POT5, POT6, POT7};
//const int POT_PINS[NUM_OF_POTS] = { POT0, POT1, POT2, POT3, 
//                                    POT4, POT5, POT6, POT7 };


//LED Analog Write Pins for adjusting LED brightness (range = 0-->1023)
const int LED0 = 9; //Value across LED0
const int LED1 = 8; //Value across LED1
const int LED2 = 7; //Value across LED2
const int LED3 = 6; //Value across LED3
const int LED4 = 5; //Value across LED4
const int LED5 = 4; //Value across LED5
const int LED6 = 3; //Value across LED6
const int LED7 = 2; //Value across LED7
const int LED_PINS[NUM_OF_POTS] = { LED0, LED1, LED2, LED3,
                                    LED4, LED5, LED6, LED7};


//Names for printing each LED power on Serial Monitor
const char* NAMES[8] = {"A_p", "A_c", "B", "C", 
                                  "D_p", "D_c", "E", "F"};

//const int LED_PINS[NUM_OF_POTS] = { LED0, LED1, LED2, LED3, 
 //                                   LED4, LED5, LED6, LED7 };                       

//---------------------------------------------------------------------------------


//----------------------- Initializing Variables ----------------------------------
//Timer
Timer timer;

//LED raw analog sensor readings
//Declare volatile to retrieve variables from memory 
//This is required for variables that may have a change during a interrupt
//function which may cause a mismatch between ram and memory data
volatile int POT_readings[NUM_OF_POTS];

//LED powers and voltages after converting raw sensor readings
double LED_volts[NUM_OF_POTS];
double LED_powers[NUM_OF_POTS];

//Number of writes to Serial Monitor. Used as a key for each data row
volatile int primary_key = 1;
//---------------------------------------------------------------------------------



//---------------------------------Functions---------------------------------------

//Read analog values in the LED sensor, write to LED values array
void readLEDValues(const int POT_PINS[], int LED_Val[], const int NUM_OF_POTS) {
  for (int i = 0; i < NUM_OF_POTS; i++) {
    LED_Val[i] = analogRead(POT_PINS[i]);
  }
  return;
}


//Convert to LED sensor readings to voltages and power consumed
void convertLEDValues(int LED_vals[], double LED_volts[], double LED_powers[],
                            const int NUM_OF_POTS , const double T_TRANS) {
  for (int i = 0; i < NUM_OF_POTS; i++) {
    LED_volts[i] = (5. / 1023.) * LED_vals[i];
    LED_powers[i] = LED_volts[i]*T_TRANS; //not T_SAMPLE because only printing
                                          //to Serial Monitor every T_TRANS
  }
  return;
}

//Print array values on Serial Monitor
void print_int_array(int arr[], const int NUM_OF_POTS) {
  for (int i = 0; i < NUM_OF_POTS; i++) {
    Serial.print(arr[i]);
    Serial.print(", ");
  }
  //Serial.println(arr[N - 1]); //Println the last value
  return;
}

void printDoubleArray(double arr[], const int NUM_OF_POTS) {
  for (int i = 0; i < NUM_OF_POTS; i++) {
    Serial.print(arr[i]);
    Serial.print(", ");
  }
  return;
}

void print_LED_values(int LED_Val[], double LED_volts[], 
                          double LED_powers[], const int NUM_OF_POTS) {
  Serial.print("LED Reading Values: ");
  print_int_array(LED_Val, NUM_OF_POTS);
  Serial.print("LED Voltage Values: ");
  printDoubleArray(LED_volts, NUM_OF_POTS);
  Serial.print("LED Power Values: ");
  printDoubleArray(LED_powers, NUM_OF_POTS);
  Serial.println(" ");
  return;
}

void print_LED_volts(double LED_volts[], const int NUM_OF_POTS) {
  Serial.print("LED Voltage Values: ");
  printDoubleArray(LED_volts, NUM_OF_POTS);
  return;
}


double lightLEDs(const int LED_PINS[], double LED_volts[], 
                        const int NUM_OF_POTS) {
  double LED_write_val;
  for (int i = 0; i < NUM_OF_POTS; i++) {
    LED_write_val = (LED_volts[i]/5.0)*255;
    if (LED_volts[i] > 0.30) {
      analogWrite(LED_PINS[i], LED_write_val);
    }
    else {
      analogWrite(LED_PINS[i], 0);
    }
  }
  return LED_write_val;
  
}

void updateBrightness(){
  //Read the raw analog readings across each potentiometer
  readLEDValues(POT_PINS, POT_readings, NUM_OF_POTS);

  //Convert analog readings into voltages and powers
  convertLEDValues(POT_readings, LED_volts, LED_powers, NUM_OF_POTS, T_TRANS);

  //Output a voltage to the corresponding LED pins
  lightLEDs(LED_PINS, LED_volts, NUM_OF_POTS);
}


void printToSerial(){
  //Print out values to Serial Monitor
  Serial.print("id: ");
  Serial.print(primary_key);
  Serial.print("; ");
  Serial.print("T_int: ");
  Serial.print(T_TRANS);
  Serial.print("; ");
  
  for (int i = 0; i < NUM_OF_POTS; i++) {
    Serial.print(NAMES[i]);
    Serial.print(": ");
    Serial.print(LED_powers[i]);
    if (i < NUM_OF_POTS-1){
      Serial.print("; ");
    }
  }
  Serial.println("");
  
  //print_LED_values(POT_readings, LED_volts, LED_powers, NUM_OF_POTS);
  primary_key++;
}


//Function to output controller states to serial link with 5 decimal places
/*void Plot(){
  Serial.println(encoderPos*K_encoder,5);
  Serial.println(encoderPos_P*K_encoder_P,5);
  Serial.println(x_sg,5);
  Serial.println(x3_hat,5);
  Serial.println(x4_hat,5);
}*/

//---------------------------------------------------------------------------------



void setup() {
  Serial.begin(38400);

  //Initialize pinmodes for potentiometers and LED pins
  for (int i = 0; i < NUM_OF_POTS; i++) {
    pinMode(LED_PINS[i], OUTPUT);
    pinMode(POT_PINS[i], INPUT);
  }

  // Perform takeReading every T_SMAMPLE (timer.every takes in ms)
  timer.every(T_SAMPLE*1000, updateBrightness);
  timer.every(T_TRANS*1000, printToSerial);
  // Perform Plot every T_plot (timer.every takes in ms)
  //timer.every(T_plot*1000,Plot);

}

void loop() {

  timer.update();

}


