Hi Lisa, to run the Arduino demo follow these steps:

1) Open Arduino, and Upload the demo.ino file
  1.1) Ensure that Tools --> Board is "Arduino MEGA 2560"
  1.2) Ensure Tools --> Port is whatever port you choose that's available

2) Run serial reader script
  2.1) python3 serial_reader.py [PORT#]
  Note: Since I'm using UBUNTU, the post name might be different so just
  change line 31 to whatever your port is
  2.2) Change POST_URL on line 23 to whatever your website post URL is

3) To "save" data once you exit the program you must KeyboardInterrupt
  3.1) While serial_reader.py is running on the terminal, Enter CTR+C
  3.2) New json data will be saved to ../MicroMarket/PricingTests.usage.json

4) Run pricing model to get graphs
  4.1) python3 pricing_test.py
  4.2) Note: If I messed up the commit/branch like today please try doing 
        pythong 3 test.py instead


