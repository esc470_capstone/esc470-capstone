# ESC470 Capstone

## Directory Description
* demo:  Files to run the Arduino demo as well as the script that submits Energy block data to the blockchain and web server.
* hyperledger-fabric: Files to run the hyperledger network as well as hyperledger composer.
* MicroMarket: Files to run the django web server.

## Installation Instructions
How to install each component is listed in their respective directory.

## Deployment
1. Deploy hyperledger-composer which in turn also deploys hyperledger-fabric.
2. Deploy Django web server.
3. Install and run the arduino code with the circuit.

