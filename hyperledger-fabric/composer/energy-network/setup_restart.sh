#!/bin/bash

# Use this script if you restarted your computer and want to up the server and ledger
# Must run from esc470-capstone/hyperledger-fabric/composer/energy-network

~/fabric-tools/startFabric.sh
~/fabric-tools/createPeerAdminCard.sh

# Only if code has been changed
composer archive create -t dir -n .

composer runtime install --card PeerAdmin@hlfv1 --businessNetworkName energy-network

composer network start --card PeerAdmin@hlfv1 --networkAdmin admin --networkAdminEnrollSecret adminpw --archiveFile energy-network@0.0.1.bna --file networkadmin.card

composer card import --file networkadmin.card

composer network ping --card admin@energy-network

composer-rest-server

# When prompted, enter the following

# admin@energy-network
# Never use namespaces
# No secure
# Yes event publication
# No TLS
