'use strict';
/**
 * Write your transction processor functions here
 */


/**
 * Create a new energy block for consumption or production.
 * @param {org.acme.biznet.EnergyTransaction} trade - the transaction to be processed and built into an energy block
 * 
 */
function createEnergyBlock(trade) {
    // Create a consumption block for the consumer, label it 'g' or 'b' depending on what it is
    console.log("Consume energy");

    // Retrieve the asset registry to create the energy block on.
    return getAssetRegistry('org.acme.biznet.EnergyBlock')
        .then(function(registry) {
            console.log("Retrieved asset registry");
            var factory = getFactory();
            // Formulate the id of the new block and then instantiate it.
            var id = trade.purchaser.address + trade.timeInterval.toString(); // Change this to strftime
            var newBlock = factory.newResource('org.acme.biznet.EnergyBlock', 'EnergyBlock', id);

            // Set the user and time interval.
            newBlock.user = trade.user;
            newBlock.timeInterval = trade.timeInterval;

            if(trade.transactionType == true){
                // If the trade is a consumption of energy, set it to consumption
                console.log("Creating energy block for consumption.");
                newBlock.energyType = trade.consumeChoice;
                newBlock.quantity = -1 * trade.quantity;

                // TODO
                // based off of the consume choice, determine whether or not you need to deduct from the green energy bank.

                // TODO
                // Take from grid? If we are designing the grid in the system.
            } else {
                // If the trade is production, keep it positive.
                console.log("Creating energy block for production.");
                newBlock.quantity = trade.quantity;
            }

            // Add the block to the registry
            return registry.add(newBlock);
        });
}

