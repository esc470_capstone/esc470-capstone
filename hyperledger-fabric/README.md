# Hyperledger Fabric

## Summary
This folder contains the installation and demos for the hyperledger fabric blockchain component of the project.  

## Directory
* bin: Used to download and install a standalone version of hyperledger fabric WITHOUT hyperledger composer.
* composer/energy-network: Contains the deployment scripts, chaincode and logic for the Energy Blockchain network.
* fabric-samples: Starter tutorials for hyperledger-fabric as well as previous setup of the energy network WITHOUT hyperledger-composer.

## Installation
For the demo, install hyperledger fabric WITH hyperledger composer.  Follow the following steps in order to complete the installation.

### Install the Prerequisites
Click the following link to install the prerequisites for your operating system:
https://hyperledger.github.io/composer/installing/installing-prereqs

### Install the developer environemnt
You will need to install the following things:
1. Essential CLI tools
``` npm install -g composer-cli ```
2. The utility for the REST cli server
```npm install -g composer-rest-server```
3. Other utility for hyperledger composer
```npm install -g generator-hyperledger-composer```
4. Yeoman (not actually needed unless you want to generate a new application.
```npm install -g yo```
5. Install composer playground
```npm install -g composer-playground```
6. Install hyperledger fabric
```
mkdir ~/fabric-tools && cd ~/fabric-tools

curl -O https://raw.githubusercontent.com/hyperledger/composer-tools/master/packages/fabric-dev-servers/fabric-dev-servers.zip
unzip fabric-dev-servers.zip
```
Once you have the downloaded fabric tools scripts, run the downloadFabric.sh script to get the docker images.
```
cd ~/fabric-tools
./downloadFabric.sh
```

## Setting up the Environment
First create peer and admin cards.
```
    cd ~/fabric-tools
    ./startFabric.sh
    ./createPeerAdminCard.sh
```