#!/bin/bash

echo "export GOROOT=/usr/local/go" >> /root/.profile
echo "export GOPATH=$HOME/go" >> /root/.profile
echo "export PATH=$GOPATH/bin:$GOROOT/bin:$PATH" >> /root/.profile
echo "export GOPATH=$HOME/go" >> /root/.bashrc
echo "export PATH=$PATH:$GOPATH/bin" >> /root/.bashrc

source /root/.bashrc

/usr/sbin/sshd -D
