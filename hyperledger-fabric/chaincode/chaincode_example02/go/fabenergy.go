package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
  "bytes"
  "encoding/json"
  "fmt"
  "strconv"

  "github.com/hyperledger/fabric/core/chaincode/shim"
  sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
type SmartContract struct {
}

type EnergyBlock struct {
  Amount          string `json:"amount"`
  IntervalStart   string `json:"interval"`
  EnergyType      string `json:"energyType"`
  User            string `json:"user"`
}

/*
 * The Init method is called when the Smart Contract "fabenergy" is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 */
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
  return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "fabenergy"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {
  fmt.Println("TESTING!")
  // Retrieve the requested Smart Contract function and arguments
  function, args := APIstub.GetFunctionAndParameters()
  // Route to the appropriate handler function to interact with the ledger appropriately
  if function == "queryEnergy" {
    return s.queryEnergy(APIstub, args)
  } else if function == "initLedger" {
    return s.initLedger(APIstub)
  } else if function == "createBlock" {
    return s.createBlock(APIstub, args)
  } else if function == "queryAll" {
    return s.queryAll(APIstub)
  } else if function == "queryPastDate" {
    return s.queryPastDate(APIstub, args)
  } else if function == "changeBlockUser" {
    return s.changeBlockUser(APIstub, args)
  }

  return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) queryPastDate(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
  if len(args) != 1 {
    return shim.Error("Incorrect number of arguments. Expecting date argument")
  }

  blockAsBytes, _ := APIstub.GetState(args[0])
  return shim.Success(blockAsBytes)
}

func (s *SmartContract) queryEnergy(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

  if len(args) != 1 {
    return shim.Error("Incorrect number of arguments. Expecting 1")
  }

  blockAsBytes, _ := APIstub.GetState(args[0])
  return shim.Success(blockAsBytes)
}

func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
  // Example chain, A, B consumers, C is prosumer
  blocks := []EnergyBlock{
   EnergyBlock{Amount: "100", IntervalStart: "1", EnergyType: "g", User: "A" },
   EnergyBlock{Amount: "200", IntervalStart: "2", EnergyType: "b", User: "B" },
   EnergyBlock{Amount: "300", IntervalStart: "3", EnergyType: "g", User: "C" },
   EnergyBlock{Amount: "100", IntervalStart: "4", EnergyType: "g", User: "A" },
   EnergyBlock{Amount: "700", IntervalStart: "5", EnergyType: "g", User: "C" },
   EnergyBlock{Amount: "200", IntervalStart: "6", EnergyType: "b", User: "B" },
   EnergyBlock{Amount: "400", IntervalStart: "7", EnergyType: "b", User: "A" },
   EnergyBlock{Amount: "300", IntervalStart: "8", EnergyType: "g", User: "B" },
   EnergyBlock{Amount: "200", IntervalStart: "9", EnergyType: "b", User: "A" },
   EnergyBlock{Amount: "500", IntervalStart: "10", EnergyType: "g", User: "C" },
  }

  // blocks := []EnergyBlock{}

  i := 0
  for i < len(blocks) {
    fmt.Println("i is ", i)
    blockAsBytes, _ := json.Marshal(blocks[i])
    APIstub.PutState("B"+strconv.Itoa(i), blockAsBytes)
    fmt.Println("Added", blocks[i])
    i = i + 1
  }

  return shim.Success(nil)
}

func (s *SmartContract) createBlock(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

  if len(args) != 5 {
    return shim.Error("Incorrect number of arguments. Expecting 6")
  }

  var block = EnergyBlock{Amount: args[1], IntervalStart: args[2], EnergyType: args[3], User: args[4] }

  blockAsBytes, _ := json.Marshal(block)
  APIstub.PutState(args[0], blockAsBytes)

  return shim.Success(nil)
}

func (s *SmartContract) queryAll(APIstub shim.ChaincodeStubInterface) sc.Response {

  startKey := "B0"
  endKey := "B999"

  resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
  if err != nil {
    return shim.Error(err.Error())
  }
  defer resultsIterator.Close()

  // buffer is a JSON array containing QueryResults
  var buffer bytes.Buffer
  buffer.WriteString("[")

  bArrayMemberAlreadyWritten := false
  for resultsIterator.HasNext() {
    queryResponse, err := resultsIterator.Next()
    if err != nil {
      return shim.Error(err.Error())
    }
    // Add a comma before array members, suppress it for the first array member
    if bArrayMemberAlreadyWritten == true {
      buffer.WriteString(",")
    }
    buffer.WriteString("{\"Key\":")
    buffer.WriteString("\"")
    buffer.WriteString(queryResponse.Key)
    buffer.WriteString("\"")

    buffer.WriteString(", \"Record\":")
    // Record is a JSON object, so we write as-is
    buffer.WriteString(string(queryResponse.Value))
    buffer.WriteString("}")
    bArrayMemberAlreadyWritten = true
  }
  buffer.WriteString("]")

  fmt.Printf("- queryAll:\n%s\n", buffer.String())

  return shim.Success(buffer.Bytes())
}

func (s *SmartContract) changeBlockUser(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

  if len(args) != 2 {
    return shim.Error("Incorrect number of arguments. Expecting 2")
  }

  blockAsBytes, _ := APIstub.GetState(args[0])
  block := EnergyBlock{}

  json.Unmarshal(blockAsBytes, &block)
  block.User = args[1]

  blockAsBytes, _ = json.Marshal(block)
  APIstub.PutState(args[0], blockAsBytes)

  return shim.Success(nil)
}

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

  // Create a new Smart Contract
  err := shim.Start(new(SmartContract))
  if err != nil {
    fmt.Printf("Error creating new Smart Contract: %s", err)
  }
}
