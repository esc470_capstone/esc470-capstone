from django.apps import AppConfig


class MicromarketappConfig(AppConfig):
    name = 'MicroMarketApp'
