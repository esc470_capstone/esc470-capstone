"""MicroMarket URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views

urlpatterns = [
    # Main view
    path('', views.index, name='index'),

    # User management
    path('manage_users', views.manage_users, name='manage users'),
    path('new_user', views.new_edit_user, name='new user'),
    path('edit_user/<int:user_id>', views.new_edit_user, name='edit user'),
    path('delete_user/<int:user_id>', views.delete_user, name='delete user'),

    # Look at graphs of green price and green usage
    path('green_info', views.green_info, name='green info'),

    # Update database
    path('change_tou', views.change_tou, name='change time-of-use prices'),
    path('upload_data', views.upload_data, name='upload data'),
    path('update', views.update, name='update costs and achievements'),
    path('smart_contract', views.smart_contract_wrapper),
    
    # Call API (debug only!)
    path('API', views.API_view)
]
