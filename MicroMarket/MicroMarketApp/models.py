#from django.contrib.auth.models import User
from django.db import models

# These may be useful in the future
# https://docs.djangoproject.com/en/2.0/topics/auth/default/#user-objects
# https://pypi.python.org/pypi/django-timeseries/1.0.1



###############################################################################
# Main user-based data models
class UserInfo(models.Model):
    #user    = models.ForeignKey(User, on_delete=models.CASCADE)
    username = models.CharField(blank=False, null=False, max_length=100, unique=True)
    address  = models.CharField(blank=False, null=False, max_length=500)

    # Usage settings
    # Whether or not to prioritize green energy (i.e. microgrid energy) 
    prioritize_green_energy = models.BooleanField(blank=False, null=False)    

    # The maximum price user is willing to pay for green energy
    max_green_energy_price  = models.FloatField(blank=False, null=False)


class UsageRawData(models.Model):
    user        = models.ForeignKey(UserInfo, on_delete=models.CASCADE)
    time_of_use = models.DateTimeField(blank=False, null=False)

    # Amount in kWH used; negative indicates power produced
    amount      = models.FloatField()

    # True if energy was from grid, false if energy from microgrid (green energy)
    grid_energy = models.BooleanField()



###############################################################################
# Models to store prices

# Store historical prices of microgrid-based energy
class HistoricalMicrogridPrice(models.Model):
    # The stated price applies to all energy used between this start time and
    # the start time of the next entry
    start = models.DateTimeField(blank=False, null=False, unique=True)
    price = models.FloatField(blank=False, null=False) # $ per kWH


# Fixed time-of-use price for macrogrid-based energy
class TimeOfUsePrice(models.Model):
    # Source: http://www.ontario-hydro.com/current-rates
    
    hour  = models.IntegerField(blank=False, null=False, unique=True) # 0 to 23
    price = models.FloatField(blank=False, null=False) # $ per kWH



###############################################################################
# Achievements
class Milestones(models.Model):
    types = [{'type': 'P', 'threshold': 20}, # Produced 500 KWH of energy
             {'type': 'P', 'threshold': 100},
             {'type': 'CG', 'threshold': 20}, # Consumed 500 KWH of green energy
             {'type': 'CG', 'threshold': 100}
    ]
    
    user = models.ForeignKey(UserInfo, on_delete=models.CASCADE)
   
    # Indicates which milestone it is (see enum above)
    milestone = models.CharField(blank=False, null=False, max_length=20)
   
    class Meta:
        # User can't have same milestone twice
        unique_together=(('user', 'milestone'))


class Rankings(models.Model):
    # Energy use = Energy used - Energy produced
    # Ranked from low to high
    ENERGY_SAVER_RANK = 1

    # Green rating = Green energy used / (brown energy used - energy produced)
    # Ranked from high to low
    GREEN_USER_RANK   = 2

    user = models.ForeignKey(UserInfo, on_delete=models.CASCADE)
   
    # Indicates which ranking type it is (see enum above)
    rank_type = models.IntegerField(blank=False, null=False)
    rank      = models.IntegerField(blank=False, null=False)

    class Meta:
        # There is only one ranking for each user
        unique_together=(('user', 'rank_type'))



###############################################################################
# Other models

# Contains amount of green energy available at the specified date and time
class GreenBank(models.Model):
    amt      = models.FloatField(null=False)
    datetime = models.DateTimeField(null=False)
    
# Contains time of last update
class LastUpdated(models.Model):
    last_updated = models.DateTimeField(blank=False, null=False)

# Allows json logs to be uploaded (these are deleted immediately after usage)
class Log(models.Model):
    log_file = models.FileField()
