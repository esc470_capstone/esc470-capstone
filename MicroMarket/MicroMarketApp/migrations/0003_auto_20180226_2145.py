# Generated by Django 2.0.2 on 2018-02-27 02:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MicroMarketApp', '0002_log'),
    ]

    operations = [
        migrations.AlterField(
            model_name='milestones',
            name='milestone',
            field=models.CharField(max_length=20),
        ),
    ]
