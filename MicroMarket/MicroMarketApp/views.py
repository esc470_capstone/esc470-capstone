import datetime
from django.conf import settings
from django.db.models import Q, Sum
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models.functions import Coalesce
from django.template import loader
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt
import json
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
from .models import UserInfo, UsageRawData, Milestones, Rankings, \
                   HistoricalMicrogridPrice, TimeOfUsePrice, Log, \
                   LastUpdated, GreenBank
import mpld3
import os
import requests
    


###############################################################################
# Constants for pricing
HISTORY_NUM     = 6    # Number of consumption data points to look at to gauge
                       # demand for green energy, including brown energy consumption;
                       # if the last points are all brown-energy related, no data is used

PRICING_CONST   = 1.0  # Minimum production/consumption ratio to induce price decrease
PRICING_STEP    = 0.05 # How aggressive the pricing adjustment is (higher = more aggressive)
ZERO_CONS_RATIO = 3    # Production/consumption ratio to use when we have no
                       # recent historical data on green energy consumption

# Default green price is always lower by the current brown energy price by
# this amount
DEFAULT_GREEN_PRICE_DIFF = 0.001



###############################################################################
# Functions to retrieve data
def GetAchievements(user_id):
    """ Returns a dict of all the user's achievements. """
    
    energy_saver_rank = Rankings.objects.filter(user__id=user_id, 
                        rank_type=Rankings.ENERGY_SAVER_RANK).values('rank')
    if energy_saver_rank:
        energy_saver_rank = energy_saver_rank[0]
    
    green_user_rank = Rankings.objects.filter(user__id=user_id, 
                      rank_type=Rankings.GREEN_USER_RANK).values('rank')
    if green_user_rank:
        green_user_rank = green_user_rank[0]
    
    achievements = {'milestones': Milestones.objects.filter(user__id=user_id),
                    'energy_saver_rank': energy_saver_rank,
                    'green_user_rank': green_user_rank
    }
        
    return achievements


def GetBill(user_id, start_time, end_time):
    """
    Returns total costs incurred by user from start_time to end_time.
    """

    all_usage = UsageRawData.objects.filter(user__id=user_id,
                                           time_of_use__gte=start_time, 
                                            time_of_use__lte=end_time).order_by('-time_of_use')

    total_cost = 0

    for usage in all_usage:
        if usage.grid_energy == False: # Use microgrid prices
            price_query = HistoricalMicrogridPrice.objects.filter(start__lte=usage.time_of_use)
        else: # Use time-of-use prices (grid)
            price_query = TimeOfUsePrice.objects.filter(hour=usage.time_of_use.hour)
            
        price = (0 if not price_query else price_query[0].price)
 
        total_cost += usage.amount * price        
    
    return total_cost


def GetCurrentGreenBank():    
    total_green_available = GreenBank.objects.all().order_by('-datetime')
    if total_green_available:
        total_green_available = total_green_available[0].amt
    else:
        total_green_available = 0.0

    return total_green_available


def GetHistoricalInfo(user_id, data_type, from_grid, start_time, end_time):
    """     
    Returns a queryset of UsageRawData objects associated with user between
    start_time and end_time, sorted by start_time (most recent to oldest)
    
    data_type is consumption or production
    """

    if data_type == 'production':
        return UsageRawData.objects.filter(user__id=user_id,
                                           amount__lte=0, 
                                           time_of_use__gte=start_time, 
                                           time_of_use__lte=end_time).order_by('-time_of_use')
    else:
        if not data_type == 'consumption':
            warning = "[**WARNING**] Unknown data type. Data type specified" + \
                  " must be 'consumption' or 'production'. Defaulting to" + \
                  " 'consumption'"
            print (warning)
        return UsageRawData.objects.filter(user__id=user_id,
                                           amount__gte=0,
                                           grid_energy=from_grid,
                                           time_of_use__gte=start_time, 
                                           time_of_use__lte=end_time).order_by('-time_of_use')


def GetRealTimeCosts():
    """ Returns current cost of green and brown energy in $ / kWH """    
    
    brp_query = TimeOfUsePrice.objects.filter(hour=datetime.datetime.now().hour)
    grp_query = HistoricalMicrogridPrice.objects.all().order_by('-start')

    brown_price = (0 if not brp_query else brp_query[0].price)
    green_price = (0 if not grp_query else grp_query[0].price)
    
    return {'brown_price': '{:.3f}'.format(brown_price),
            'green_price': '{:.3f}'.format(green_price)}    


def GetUsageSettings(user_id):
    return UserInfo.objects.filter(id=user_id).values('prioritize_green_energy',
                                              'max_green_energy_price')[0]


def GetUserInfo(user_id, start_time, end_time, last_billing_date):
    """ 
    Gets user info in the form of a dict.
    
    start_time and end_time are the interval for which usage info is obtained.
    
    last_billing_date is used to calculate the bill; bill takes into account 
    all usage since the last billing date.
    """
    production        = GetHistoricalInfo(user_id, 'production', False, start_time, end_time)
    consumption_green = GetHistoricalInfo(user_id, 'consumption', False, start_time, end_time)
    consumption_brown = GetHistoricalInfo(user_id, 'consumption', True, start_time, end_time)
    bill              = GetBill(user_id, last_billing_date, end_time)
    settings          = GetUsageSettings(user_id)
    achievements      = GetAchievements(user_id)        

    fig = plt.figure(figsize=(5,3.5), dpi=100)

    if production:
        plt.plot_date(list(p.time_of_use for p in production), 
                      list((-1.0)*p.amount for p in production), 
                      marker='X', markersize=8, color='#ff66cc', label='produced')
    if consumption_green:
        plt.plot_date(list(cg.time_of_use for cg in consumption_green), 
                      list(cg.amount for cg in consumption_green), 
                      marker='o', markersize=8, color='#66ff66', label='used (green)')
    if consumption_brown:
        plt.plot_date(list(cb.time_of_use for cb in consumption_brown), 
                      list(cb.amount for cb in consumption_brown), 
                      marker='o', markersize=8, color='#3366ff', label='used (macrogrid)')
        
    plt.title('Energy Usage')
    plt.ylabel('Energy (KWH)')
    plt.grid(True) 
    plt.legend()

    # Set all plots to start and end at same time for consistency   
    plt.axis([start_time, end_time, None, None])
    ax = plt.gca()
    ax.set_autoscale_on(False)

    # Set fixed number of x ticks to avoid crowding the axis
    ax.xaxis.set_major_locator(MaxNLocator(6))

    # Convert to html format
    figure = mpld3.fig_to_html(fig, template_type='general')
    plt.close(fig)

    usage_info_dict = {'user_id': user_id,
                       'user_info': UserInfo.objects.get(pk=user_id),
                       'bill': '{:.2f}'.format(bill),
                       'settings': settings,
                       'achievements': achievements,
                       'figure': figure,
    }
    
    return usage_info_dict


###############################################################################
# Functions to update the database
def ImportDataFromJson(item, isfile=True):
    """
    Imports data from the .json file or string into the database. Return value 
    is whether importing succeeded.

    item: the absolute path to the .json file if isfile=True
          OR the json string if isfile=False
    """

    json_data = None
    
    try:
        if isfile: # Load from file
            json_file = open(item)    
            json_data = json.load(json_file)
        else:
            json_data = json.loads(item)
    except:
        return False

    # Example data:
    # {'user': 'resource:org.acme.biznet.User#A',
    #  '$class': 'org.acme.biznet.EnergyBlock',
    #  'energyType': 'g', 
    #  'quantity': -300, 
    #  'timeInterval': '2018-03-28T00:00:00.000Z', 
    #  'id': 'someid'}

    for data in json_data:
        username = data['user'].split('#')[-1]
        timeUsed = datetime.datetime.strptime(data['timeInterval'], '%Y-%m-%dT%H:%M:%S.%fZ')
    
        usage_obj = UsageRawData(user=UserInfo.objects.filter(username=username)[0],
                                 time_of_use=timeUsed,
                                 amount=float(data['quantity']),
                                 grid_energy = (data['energyType'] == "b"))
        usage_obj.save()
    
    return True


def UpdateAchievements():
    """ 
    For all users, award new milestones (if applicable) and update rankings. 
    """

    agg_usage = UsageRawData.objects.values('user__id').annotate(
                    amt_produced=Coalesce(-1*Sum('amount', filter=Q(amount__lt=0)), 0),
                    amt_green_used=Coalesce(Sum('amount', filter=Q(amount__gt=0, grid_energy=False)), 0),
                    amt_brown_used=Coalesce(Sum('amount', filter=Q(amount__gt=0, grid_energy=True)), 0),
                    amt_total=Coalesce(Sum('amount'), 0)) 
    
    milestone_dict = {}
    
    for info in Milestones.types:
        milestone_name = info['type'] + str(info['threshold'])        
        
        if info['type'] == 'P':
            milestone_dict[milestone_name] = agg_usage.filter(amt_produced__gte=info['threshold'])
        elif info['type'] == 'CG':
            milestone_dict[milestone_name] = agg_usage.filter(amt_green_used__gte=info['threshold'])

    # Calculations are based on model info from models.Rankings
    rankings_dict = {Rankings.ENERGY_SAVER_RANK: sorted(agg_usage, key=lambda a: a['amt_total']),
                     Rankings.GREEN_USER_RANK: sorted(agg_usage, key=lambda a: 
                             (a['amt_green_used'] / (a['amt_brown_used'] - a['amt_produced'] + 0.001)), reverse=True)
    }

    # The 0.001 term is to prevent division by zero errors; if the person has
    # produced the same amount of energy as brown energy used, they will end
    # up with a very large rating (i.e. good ranking)

    for milestone_type, users in milestone_dict.items():
        
        if users:
            for user in users:
                # Only add milestone if they don't already have it
                if not Milestones.objects.filter(user__id=user['user__id'], milestone=milestone_type):
                    milestone_object = Milestones(user=UserInfo.objects.get(pk=user['user__id']),
                                                  milestone=milestone_type)
                    milestone_object.save()
            
    for ranking_type, user in rankings_dict.items():
        for rank, user in enumerate(user, start=1):
            
            ranking_object = Rankings.objects.filter(user__id=user['user__id'], rank_type=ranking_type)
            
            if ranking_object: # If a ranking object exists, update it
                ranking_object = ranking_object[0]
                ranking_object.rank = rank
            else: # Make a new ranking object
                ranking_object = Rankings(user=UserInfo.objects.get(pk=user['user__id']),
                                          rank_type=ranking_type,
                                          rank=rank)
            ranking_object.save()
    return


def UpdateMicrogridEnergyPrice():
    current_green_price = HistoricalMicrogridPrice.objects.all().order_by('-start')

    if current_green_price:
        current_green_price = current_green_price[0].price
    else: # No current price; set it slightly lower than current brown energy price
        current_brown_price = TimeOfUsePrice.objects.filter(hour=datetime.datetime.now().hour)[0].price
        current_green_price = current_brown_price - DEFAULT_GREEN_PRICE_DIFF

    total_green_available = GetCurrentGreenBank()

    # Get most recent consumption transactions
    last_cons = UsageRawData.objects.filter(amount__gte=0).order_by('-time_of_use')

    if len(last_cons) >= HISTORY_NUM:
        last_cons = last_cons[:HISTORY_NUM] # Look at most recent ones
   
    total_green_cons = 0

    for cons in last_cons:
        if not cons.grid_energy:
            total_green_cons += cons.amount

    if not total_green_cons: # No recent green consumption
        prod_cons_ratio = ZERO_CONS_RATIO
    else:
        prod_cons_ratio = total_green_available / total_green_cons

    new_price = max(0, # Ensures price never goes negative 
                    current_green_price + PRICING_STEP * (PRICING_CONST - prod_cons_ratio))

    # Save the new price
    price_object = HistoricalMicrogridPrice(start=datetime.datetime.now(),
                                            price=new_price)
    price_object.save() 

    return new_price



###############################################################################
# Functions to interact with blockchain
def MakeBlock(block_data):
    """
    Makes a block in the blockchain with the specified data.
    """
    block_url = 'http://localhost:3000/api/EnergyBlock'

    requests.post(block_url, json=block_data)


def SmartContract(event, timestamp):
    """
    event: JSON data containing details (user, amount, etc.) of a usage event

    Using the user's energy preferences, this function determines which type of 
    energy the user needs to buy (green or brown) and creates the appropriate 
    entries in the blockchain.
    
    If energy was produced instead of consumed, the entry is directly entered
    into the blockchain.
    """   
    current_brown_price = float(GetRealTimeCosts()['brown_price'])
    current_green_price = float(GetRealTimeCosts()['green_price'])
    
    new_total_green_available = GetCurrentGreenBank()

    user_settings = UserInfo.objects.filter(username=event['username'])[0]

    prioritize_green = user_settings.prioritize_green_energy
    max_green_price  = user_settings.max_green_energy_price

    timestring = timestamp.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
    
    # Initialize block data 
    block_data = {"$class": "org.acme.biznet.EnergyBlock",
                  "user" : "resource:org.acme.biznet.User#" + event['username'],
                  "timeInterval": timestring,
                  
                  "id" : event['username'] + timestring,
                  "quantity": None,
                  "energyType": None
    }

    if int(event['producing']):
        block_data['energyType'] = 'g'
        block_data['quantity'] = -1.0 * event['amount'] # Negative consumption = generation

        MakeBlock(block_data)   
        new_total_green_available += float(event['amount'])
    else:
        user_wants_green = prioritize_green and max_green_price >= current_green_price
        green_is_cheaper = current_green_price <= current_brown_price
        
        use_green = (user_wants_green or green_is_cheaper) and new_total_green_available
        
        if use_green:
            # Need more green energy than available
            if float(event['amount'] > new_total_green_available):
                amt_bought = new_total_green_available
                amt_left   = float(event['amount']) - amt_bought
                new_total_green_available = 0

                # Buy all green energy
                block_data['quantity'] = amt_bought
                block_data['energyType'] = 'g'
                MakeBlock(block_data)
        
                # Buy remaining energy from grid
                block_data['quantity'] = amt_left
                block_data['energyType'] = 'b'
                
                # Hack: need to ensure usage time is not identical or else blockchain will reject the block
                timestampNew  = timestamp + datetime.timedelta(milliseconds=1)
                timestringNew = timestampNew.strftime('%Y-%m-%dT%H:%M:%S.%fZ') 

                block_data['timeInterval'] = timestringNew
                block_data['id'] = event['username'] + timestringNew
                
                MakeBlock(block_data)
            else:
                amt_bought = float(event['amount'])
                new_total_green_available -= amt_bought
                
                # Buy green energy
                block_data['quantity'] = amt_bought
                block_data['energyType'] = 'g'
                MakeBlock(block_data)

        else: # Buy from grid
            block_data['quantity'] = event['amount']
            block_data['energyType'] = 'b'
            MakeBlock(block_data)

    return new_total_green_available


def UpdateDBFromBlockchain():
    
    most_recent_update = LastUpdated.objects.all().order_by('-last_updated')

    url = "http://localhost:3000/api/queries/selectBlocksPastDate?time="

    if most_recent_update:
        most_recent_update = most_recent_update[0]
        url += most_recent_update.last_updated.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
    else:
        url += '0' # There is no most recent update; get all blocks since epoch
        
    blockinfo = requests.get(url)

    ImportDataFromJson(json.dumps(blockinfo.json()), isfile=False)

    # Add new entry to "last updated" table
    last_updated_obj = LastUpdated(last_updated=datetime.datetime.now())
    last_updated_obj.save()

    return blockinfo.json()



###############################################################################
# Views

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def change_tou(request):
    tou_list = []

    for hour in range(0, 24):
        hour_query = TimeOfUsePrice.objects.filter(hour=hour)
        if request.POST:
            key   = 'price_' + str(hour)
            price = float(request.POST[key])
            
            if not hour_query:
                tou_object = TimeOfUsePrice(hour=hour, price=price)
            else:
                tou_object = hour_query[0]
                tou_object.price = price
            tou_object.save()
        else:
            price = 0
            if hour_query:
                price = hour_query[0].price

        tou_list.append({'hour': hour, 'price': price})

    context = {'tous': tou_list,
               'successfully_saved': (not not request.POST)}
    
    template = loader.get_template('MicroMarketApp/change_tou.html')
    return HttpResponse(template.render(context, request))


def delete_user(request, user_id):
    user_query = UserInfo.objects.filter(id=user_id)
    
    if user_query:
        user_object = user_query[0]
        user_object.delete()

    return HttpResponseRedirect("/MicroMarketApp/manage_users")


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def green_info(request):
    """ 
    Plot historical green energy prices.
    """  
    # Plot green price history info
    green_price_history = HistoricalMicrogridPrice.objects.all().order_by('start')

    fig = plt.figure(figsize=(5,3.5), dpi=100)
    plt.plot_date(list(gp.start for gp in green_price_history), 
                  list(gp.price for gp in green_price_history), 
                      marker='o', markersize=8, color='#66ff66')
        
    plt.ylabel('Price (Dollars)')
    plt.grid(True)

    # Set all plots to start and end at same time for consistency  
    #plt.axis([start_time, end_time, None, None])
    ax = plt.gca()
    ax.set_autoscale_on(False)
    ax.xaxis.set_major_locator(MaxNLocator(6))
    green_price_figure = mpld3.fig_to_html(fig, template_type='general')
    plt.close(fig)
    
    context = {'green_price_figure': green_price_figure
    }

    template = loader.get_template('MicroMarketApp/green_info.html')
    return HttpResponse(template.render(context, request))


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def index(request):
    this_month = datetime.datetime.now().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
    # Default values
    start_time        = this_month
    end_time          = datetime.datetime.now()
    last_billing_date = this_month

    warning = ''
    
    if request.POST:
        try:
            start_time        = datetime.datetime.strptime(request.POST['start_time'], '%Y-%m-%d %I:%M %p')
            end_time          = datetime.datetime.strptime(request.POST['end_time'], '%Y-%m-%d %I:%M %p')
            last_billing_date = datetime.datetime.strptime(request.POST['last_billing_date'], '%Y-%m-%d')
        except:
            warning = 'One or more of the date / times entered are invalid!'

    # List of dicts containing user info
    usage_infos = []
    
    for user in UserInfo.objects.all().order_by('id'):
        usage_infos.append(GetUserInfo(user.id, start_time, end_time, last_billing_date))

    real_time_costs = GetRealTimeCosts()

    context = {'usage_infos': usage_infos,
               'real_time_costs': real_time_costs,
               'start_time': start_time.strftime("%Y-%m-%d %I:%M %p"),
               'end_time': end_time.strftime("%Y-%m-%d %I:%M %p"),
               'last_billing_date': last_billing_date.strftime("%Y-%m-%d"),
               'warning': warning 
    }

    template = loader.get_template('MicroMarketApp/index.html')
    return HttpResponse(template.render(context, request))


def manage_users(request):
    context = {'user_infos': UserInfo.objects.all()}
    
    template = loader.get_template('MicroMarketApp/manage_users.html')
    return HttpResponse(template.render(context, request))


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def new_edit_user(request, user_id=None):

    user_object = None
    context     = {}
    
    if user_id:
        user_object = UserInfo.objects.get(pk=user_id)

    if request.POST:
        if user_id: # Edit existing user
            user_object = UserInfo.objects.get(pk=user_id)

            user_object.username = request.POST['username']
            user_object.address  = request.POST['address']

            user_object.prioritize_green_energy = ('prioritize_green_energy' in request.POST.keys())
            user_object.max_green_energy_price  = request.POST['max_green_energy_price']
        else: # Add new user
            user_object = UserInfo(username=request.POST['username'],
                                   address=request.POST['address'],
                                   prioritize_green_energy=('prioritize_green_energy' in request.POST.keys()),
                                   max_green_energy_price=request.POST['max_green_energy_price'])
        user_object.save()

        context['successfully_saved'] = True

    context['user'] = user_object

    template = loader.get_template('MicroMarketApp/new_edit_user.html')
    return HttpResponse(template.render(context, request))


@csrf_exempt 
def smart_contract_wrapper(request):
    """ 
    This functions expects a request containing JSON data from a power meter.
    
    For each entry in the JSON data, it calls the smart contract.
    """

    if not request.POST: # Nothing to see
        return HttpResponse("No post info provided")
    
    usage_data = request.POST
    
    for timestamp_key in sorted(usage_data.keys()):

        events = json.loads(request.POST[timestamp_key])  

        timestamp = datetime.datetime.strptime(timestamp_key, "%Y-%m-%d %H:%M:%S")
    
        for event in events:
            new_green_available = SmartContract(event, timestamp)
            new_green_bank_obj = GreenBank(amt=new_green_available,
                                           datetime=datetime.datetime.now())
            new_green_bank_obj.save()
    
    return HttpResponse("Done calling smart contract wrapper")


def update(request):
    """
    This function can be manually called via webserver or periodically called
    via crontab.
    """

    UpdateDBFromBlockchain()
    UpdateMicrogridEnergyPrice()
    UpdateAchievements()

    return HttpResponse("Done updating")


# For debugging only / old
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def upload_data(request):    
    context = {}
    
    if request.POST:
        log_object = Log(log_file=request.FILES['json_file'])
        log_object.save()

        log_path = os.path.join(settings.MEDIA_ROOT, str(log_object.log_file))
        
        if ImportDataFromJson(log_path):
            status = "Successfully updated database using JSON file!"
        else:
            status = "Failed to update database! The file is not present or not valid JSON!"
        
        log_object.delete()
        os.remove(log_path)
        
        context['status'] = status

    template = loader.get_template('MicroMarketApp/upload_data.html')
    return HttpResponse(template.render(context, request))        


# For debugging only
def API_view(request):
    context = {}
    template = loader.get_template('MicroMarketApp/API.html')

    url = "http://localhost:3000/api/queries/selectBlocksPastDate?time=0"
    blockinfo = requests.get(url)   
    
    context['json'] = blockinfo.json()

    return HttpResponse(template.render(context, request))