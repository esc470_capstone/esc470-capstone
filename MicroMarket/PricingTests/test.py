# vim: set expandtab tabstop=2 shiftwidth=2 softtabstop=2 foldmethod=marker:
import json
from matplotlib import pyplot as plt
import matplotlib.dates as md

user_file  = open('users.json')
usage_file = open('usage.json')
user_data  = json.load(user_file)
usage_data = json.load(usage_file)

users         = {}
brown_price         = 0.157
default_green_price = brown_price - 0.001
current_green_price = default_green_price
green_data = {'total_green_available': 0, # Amount of available green energy
              'current_green_price': default_green_price
}

DATE_TO_STRING = md.DateFormatter('%Y-%m-%d %H:%M:%S')

# Lists for keeping track of values for plotting
total_green_availables = []
green_prices           = []
usage_blocks           = []

# Algorithm variables
UPDATE_INTERVAL = 2    # Update green price once per X transactions
HISTORY_NUM     = 5    # Number of consumption data points to look at to gauge
                       # demand for green energy, including brown energy consumption;
                       # if the last points are all brown-energy related, no data is used
PRICING_CONST   = 1.0  # Minimum production/consumption ratio to induce price decrease
PRICING_STEP    = 0.05 # How aggressive the pricing adjustment is (higher = more aggressive)
ZERO_CONS_RATIO = 3    # Production/consumption ratio to use when last_green_cons is empty


def SetPrice(green_data, ZERO_CONS_RATIO, PRICING_STEP, \
              PRICING_CONST, printout=True):
    """ 
    Using green data information, calculate the unit price of green energy

    Inputs:
      1) green_data: hash.keys() = [
        1.1) 'total_green_available': amount of green energy in microgrid
        1.2) 'current_green_price': current unit price of green energy
        1.3) 'last_green_cons': list of recent blocks with green energy consumption 
      2) ZERO_CONS_RATIO: prod/cons ratio to use when last_green_cons is empty
      3) PRICING_STEP: slope/agressiveness of pricing adjustment
      4) PRICING_CONST: min prod/cons ratio to induce price decrease
      5) printout (Optional: default = True)
    
    Return:
      Int: new_price
    """
    total_cons = sum(float(u['amount']) for u in green_data['last_green_cons'])

    if not last_green_cons: # No recent green consumption
        prod_cons_ratio = ZERO_CONS_RATIO
    else:
        prod_cons_ratio = green_data['total_green_available'] / total_cons
    
    if printout:
      print ("Total green consumed: " + str(total_cons) + \
              " Prod/Cons ratio: " + str(prod_cons_ratio))

    new_price = max(0, #Ensures price never goes negative 
                    green_data['current_green_price'] +\
                      PRICING_STEP * (PRICING_CONST - prod_cons_ratio))

    return new_price


def MakeBlock(usage_blocks, block_data_hash, printout=True):
  
  usage_item = {'event_id': block_data_hash['event_id'],
                'timestamp': block_data_hash['timestamp'],
                'username': block_data_hash['username'],
                'amount': float(block_data_hash['amount']),
                'producing' : block_data_hash['producing'],
                'price' : block_data_hash['price'],
                'microgrid' : block_data_hash['microgrid']    
  }

  if printout:
      print (usage_item)

  usage_blocks.append(usage_item)    


def SmartContractMaker(green_data, event, user, timestamp, \
                        usage_blocks, event_id, printout=True):
  '''
  Inputs:
    1) green_data: hash.keys() = ['total_green_avaiable', 'current_green_price']
    2) event: hash.keys() = ['username', 'amount', 'producing']
    3) user: hash.keys() = ['username', 'prioritizes_green', 'max_green_price']
    4) timestamp
    5) usage_blocks: array to write historical data into
    6) event_id: unique event identifier (usually just index)
    7) printout (Optional: default = True)
  
  Returns:
    Int: new_total_green_available
  '''
  new_total_green_available = green_data['total_green_available']

  #Initialize block data 
  block_data_hash = {'username': event['username'],
                'amount': event['amount'],
                'timestamp': timestamp,
                'event_id': event_id
  } 

  if int(event['producing']):
    block_data_hash['price'] = green_data['current_green_price']
    block_data_hash['producing'] = True
    block_data_hash['microgrid'] = True

    MakeBlock(usage_blocks, block_data_hash, printout)        
    new_total_green_available += float(event['amount'])
#    print(event_id)

  else:
    block_data_hash['producing'] = False
    use_green = (( (user['prioritizes_green'] and \
                      float(user['max_green_price']) >=  
                        green_data['current_green_price']) or \
                    green_data['current_green_price'] <= brown_price)
                 and new_total_green_available > 0)

    if use_green:
      block_data_hash['price'] = green_data['current_green_price']

      if float(event['amount']) > new_total_green_available:
        amt_bought = new_total_green_available
        amt_left   = float(event['amount']) - amt_bought
        new_total_green_available = 0

        #Buy all green energy
        block_data_hash['amount'] = amt_bought
        block_data_hash['microgrid'] = True
        MakeBlock(usage_blocks, block_data_hash, printout)

        #Buy remaining energy from grid
        block_data_hash['microgrid'] = False
        block_data_hash['amount'] = amt_left
        MakeBlock(usage_blocks, block_data_hash, printout)

      else:
        amt_bought = float(event['amount'])
        new_total_green_available -= amt_bought
        
        #Buy green energy
        block_data_hash['microgrid'] = False
        block_data_hash['amount'] = amt_bought
        MakeBlock(usage_blocks, block_data_hash, printout)

    else: # Buy from grid
      block_data_hash['price'] = brown_price
      block_data_hash['producing'] = False
      block_data_hash['microgrid'] = False
      MakeBlock(usage_blocks, block_data_hash, printout) #Buy remaining left from energy bids

  return new_total_green_available



if __name__ == '__main__':
  
  #Reformat user data from JSON file to have username as a key
  for user in user_data:
    name  = user['username']
    new_info = {'prioritizes_green': (not not int(user['prioritizes_green'])),
                'max_green_price': float(user['max_green_price'])}
    users[name] = new_info

  num_of_events = 0
  price_timestamps = []

  for timestamp_key in sorted(usage_data.keys()):
    usage = json.loads(usage_data[timestamp_key])
    n = len(usage)
    for event in usage:
      # Keep historical data
      print ("\nTimestamp: " + str(timestamp_key) + \
          " Total green energy: " + str(green_data['total_green_available']))
      total_green_availables.append(green_data['total_green_available']) 

      green_data['total_green_available'] = SmartContractMaker(green_data, \
                                              event, user, timestamp_key, \
                                              usage_blocks, num_of_events, \
                                              printout=True)
      
      #Update price if we're at the update interval
      if num_of_events % UPDATE_INTERVAL:
        last_green_cons = []

        #Get the last HISTORY_NUM blocks that include 
        for (idx, event_usage) in enumerate(reversed(usage_blocks), start=1):
          if idx > HISTORY_NUM: # Only check last (X) usages
            break
          else:
            if event_usage['microgrid'] and not event_usage['producing']:
              last_green_cons.append(event_usage)

        #Update the green_data values
        green_data['last_green_cons'] = last_green_cons
        green_data['current_green_price'] = SetPrice(green_data, ZERO_CONS_RATIO,
                                                    PRICING_STEP, PRICING_CONST, 
                                                    printout=True)

        #Save historical price data
        print("New price: " + str(green_data['current_green_price']))
      
        green_prices.append(green_data['current_green_price']) # Keep historical data
        price_timestamps.append(timestamp_key)

      num_of_events += 1

# Plot consumption / green energy available / prices
  green_idx = []
  brown_idx = []
  green_consumption = []
  brown_consumption = []

  for usage in usage_blocks:
    if usage['microgrid']:
      green_idx.append(usage['event_id'])
      green_consumption.append(usage['amount'])
    else:
      brown_idx.append(usage['event_id'])
      brown_consumption.append(usage['amount'])


  fig = plt.figure()
  
  ax1 = fig.add_subplot(4,1,1)
  plt.scatter(range(len(total_green_availables)), total_green_availables, c='m')
  plt.grid(True)
  plt.ylabel('Total green energy available')

  ax2 = fig.add_subplot(4,1,2, sharex=ax1)
  plt.scatter(range(0, len(green_prices)*UPDATE_INTERVAL, UPDATE_INTERVAL), green_prices, c='r')
  plt.grid(True)
  plt.ylabel('Green prices') 

  ax3 = fig.add_subplot(4,1,3, sharex=ax1)
  plt.scatter(green_idx, green_consumption, c='g')
  plt.grid(True)
  plt.ylabel('Green consumption')

  ax4 = fig.add_subplot(4,1,4, sharex=ax1)
  plt.scatter(brown_idx, brown_consumption, c='b')
  plt.grid(True)
  plt.xlabel('Time step')
  plt.ylabel('Brown consumption')

  plt.show()
