import datetime
import json
import requests



###############################################################################
# Data
User_1 = {
  "$class": "org.acme.biznet.User",
  "address": "A", # This is the ID
  "firstName": "A",
  "lastName": "A",
  "purchaseGreen": "1",
  "maxPurchasePrice": 0,
  "type": "CONSUMER"
}

User_2 = {
  "$class": "org.acme.biznet.User",
  "address": "B",
  "firstName": "B",
  "lastName": "B",
  "purchaseGreen": "1",
  "maxPurchasePrice": 0,
  "type": "CONSUMER"
}

User_3 = {
  "$class": "org.acme.biznet.User",
  "address": "C",
  "firstName": "C",
  "lastName": "C",
  "purchaseGreen": "1",
  "maxPurchasePrice": 0,
  "type": "CONSUMER"
}

User_4 = {
  "$class": "org.acme.biznet.User",
  "address": "D",
  "firstName": "D",
  "lastName": "D",
  "purchaseGreen": "1",
  "maxPurchasePrice": 0,
  "type": "CONSUMER"
}

User_5 = {
  "$class": "org.acme.biznet.User",
  "address": "E",
  "firstName": "E",
  "lastName": "E",
  "purchaseGreen": "1",
  "maxPurchasePrice": 0,
  "type": "CONSUMER"
}

User_6 = {
  "$class": "org.acme.biznet.User",
  "address": "F",
  "firstName": "F",
  "lastName": "F",
  "purchaseGreen": "1",
  "maxPurchasePrice": 0,
  "type": "CONSUMER"
}

march_26 = datetime.datetime(2018, 3, 26)
march_28 = datetime.datetime(2018, 3, 28)
march_29 = datetime.datetime(2018, 3, 29)

Block_1 = {
  "$class": "org.acme.biznet.EnergyBlock",
  "id": "yourmom",
  "timeInterval": march_26.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
  "energyType": "b",
  "quantity": 100,
  "user": "resource:org.acme.biznet.User#A"
}

Block_2 = {
  "$class": "org.acme.biznet.EnergyBlock",
  "id": "yourdad",
  "timeInterval": march_28.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
  "energyType": "g",
  "quantity": -300,
  "user": "resource:org.acme.biznet.User#A"
}

Block_3 = {
  "$class": "org.acme.biznet.EnergyBlock",
  "id": "yourface",
  "timeInterval": march_29.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
  "energyType": "g",
  "quantity": -50,
  "user": "resource:org.acme.biznet.User#A"
}

Circuit_Data = {"2018-04-13 15:39:51": 
    [{"username": "A", "amount": 1.17, "producing": 1},
     {"username": "A", "amount": 24.95, "producing": 0}, 
     {"username": "B", "amount": 4.59, "producing": 0}, 
     {"username": "C", "amount": 2.91, "producing": 0}, 
     {"username": "D", "amount": 6.09, "producing": 1}, 
     {"username": "D", "amount": 9.43, "producing": 0}, 
     {"username": "E", "amount": 6.09, "producing": 0}, 
     {"username": "F", "amount": 24.93, "producing": 0}]}


    
###############################################################################
# Functions
def CreateUsers():
    user_url = 'http://localhost:3000/api/User'
    
    requests.post(user_url, json=User_1)
    requests.post(user_url, json=User_2)
    requests.post(user_url, json=User_3)
    requests.post(user_url, json=User_4)
    requests.post(user_url, json=User_5)
    requests.post(user_url, json=User_6)


def DeleteTestBlocks():
    ids = ['yourmom', 'yourdad', 'yourface']
    url = 'http://localhost:3000/api/EnergyBlock/'
    
    for id_del in ids:
        requests.delete(url + id_del)        

        
def GetBlocksSince(dateTime):
    url = "http://localhost:3000/api/queries/selectBlocksPastDate?time=" + dateTime
    blockinfo = requests.get(url)
    
    return blockinfo.json()


def InsertBlocks():
    block_url = 'http://localhost:3000/api/EnergyBlock'
    
    requests.post(block_url, json=Block_1)
    requests.post(block_url, json=Block_2)
    requests.post(block_url, json=Block_3)


def SendRequestFromMeter():
    django_url = 'http://localhost:8000/MicroMarketApp/smart_contract'

    # Bit of a hack; we need username and usage time to be unique for the blockchain; however,
    # this messes things up if we have two distinct entries for energy production
    # and consumption for a single username at a single timestamp. To 
    # avoid this, we will process the circuit data and slightly change
    # the timestamp for a usage entry if we have already seen a usage with the same username
    # at the same time.

    # Ideally we should change the blockchain structure but unfortunately
    # the blockchain code is mildly unstable and we don't want to upset it

    
    Alt_Data = {}    
    
    # Preprocessing
    for key in Circuit_Data:
        users_already_seen = []
        alt_time  = datetime.datetime.strptime(key, '%Y-%m-%d %H:%M:%S') + datetime.timedelta(seconds=1)   
        alt_datas = []

        for idx, usage in enumerate(Circuit_Data[key]):
            if usage["username"] not in users_already_seen:
                users_already_seen.append(usage["username"])
            else: # Need to change its timestamp
                data_to_move = Circuit_Data[key].pop(idx)
                alt_datas.append(data_to_move)

        Alt_Data[alt_time] = alt_datas
                
        Circuit_Data[key] = json.dumps(Circuit_Data[key]) # Format as str
 
    for key in Alt_Data:
        Alt_Data[key] = json.dumps(Alt_Data[key])                
                
    requests.post(django_url, data=Circuit_Data)
    requests.post(django_url, data=Alt_Data)


def TestBlocksQuery():   
    early        = datetime.datetime(2000, 1, 1)
    after_mar_26 = datetime.datetime(2018, 3, 26, 14)
    after_mar_28 = datetime.datetime(2018, 3, 28, 14)
    after_mar_29 = datetime.datetime(2018, 3, 29, 14)

    print("Expect 0 blocks")
    print (GetBlocksSince(after_mar_29.strftime('%Y-%m-%dT%H:%M:%S.%fZ')))
    
    print("Expect 1 blocks")
    print (GetBlocksSince(after_mar_28.strftime('%Y-%m-%dT%H:%M:%S.%fZ')))
    
    print("Expect 2 blocks")
    print (GetBlocksSince(after_mar_26.strftime('%Y-%m-%dT%H:%M:%S.%fZ')))
    
    print("Expect 3 blocks")
    print (GetBlocksSince(early.strftime('%Y-%m-%dT%H:%M:%S.%fZ')))
    
    print ("Expect 3 blocks?")
    print (GetBlocksSince('0'))    


###############################################################################
# Main

#InsertBlocks()
#TestBlocksQuery()
#DeleteTestBlocks()
#SendRequestFromMeter()

CreateUsers()